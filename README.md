# Test-moni

Pequeño sitio web que registra solicitudes de prestamos.


Instalación en entorno local:

Tienes que tener instalado pip3 en tu distribución linux. Con pip3 instalado:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    $ sudo pip3 install virtualenv
    $ python -m venv <path ambiente py3>
    $ source <path ambiente py3>/bin/activate
    $ python --version
    Python 3.x
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ya está instalado un ambiente para python3. Ahora debes clonar el
software usando git e instalar dependencias:


    $ git clone https://gitlab.com/wilmays10/test-moni
    $ cd test-moni
    $ pip install -r requirements.txt