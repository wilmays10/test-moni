from django.db import models
from model_utils import Choices


class Persona(models.Model):
    """
    Datos del usuario que solicita un prestamo
    """
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    genero = models.CharField(
        max_length=20,
        choices=Choices("masculino", "femenino"),
        default="masculino",
    )
    numero_documento = models.IntegerField(unique=True)
    email = models.EmailField()

    def __str__(self):
        return f"{self.apellido}, {self.nombre}"


class Prestamo(models.Model):

    solicitante = models.ForeignKey('Persona', on_delete=models.CASCADE)
    monto = models.FloatField()
    aprobado = models.BooleanField(default=False)
    errores = models.BooleanField(default=True)

    def __str__(self):
        return f"id: {self.id} - {self.solicitante} - $ {self.monto}"
