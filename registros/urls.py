"""moni_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from registros import views

urlpatterns = [
    path(
        'solicitudes',
        views.SolicitudListView.as_view(),
        name='lista_solicitudes'
    ),
    path(
        r'detalle-solicitud/<int:pk>',
        views.SolicitudDetailView.as_view(),
        name='detalles_solicitudes',
    ),
    path(
        r'editar-solicitud/<int:pk>',
        views.SolicitudUpdateView.as_view(),
        name="editar_solicitud",
    ),
    path(
        r'eliminar-solicitud/<int:pk>',
        views.SolicitudDeleteView.as_view(),
        name="eliminar_solicitud",
    ),
]
