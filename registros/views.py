from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic import UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.urls import reverse, reverse_lazy
from registros.models import Prestamo, Persona
from registros.forms import SolicitudForm
from registros.settings import CREDENTIAL, URL_API
import json
import requests


def aprobar_solicitud(dni):
    """
    Función que se conecta a API externa para verificar si una solicitud
    es aprobada
    """
    resultado = {
        'estado': False,
        'errores': False,
        'error_conexion': False
    }
    url = URL_API +str(dni)
    headers = {'Credential': CREDENTIAL}
    try:
        basedata = requests.get(url, headers=headers)
        basedata = basedata.content.decode("utf-8")
        jsondata = json.loads(basedata)
        if jsondata['status'] == 'approve':
            resultado['estado'] = True
        else:
            # si la solicitud tuvo errores
            if jsondata['has_error'] == True:
                resultado['errores'] = True

    except Exception as e:
        resultado['error_conexion'] = True

    return resultado


def solicitud_new(request):
    """
    Home
    """
    if request.method == "POST":
        form = SolicitudForm(request.POST)
        if form.is_valid():

            try:
                persona = Persona.objects.get(
                        numero_documento=form.cleaned_data['dni'])
            except Exception:
                persona = Persona.objects.create(
                            nombre = form.cleaned_data['nombre'],
                            apellido = form.cleaned_data['apellido'],
                            genero = form.cleaned_data['genero'],
                            numero_documento = form.cleaned_data['dni'],
                            email = form.cleaned_data['email']
                            )

            # la solicitud se guardará siempre
            prestamo = Prestamo.objects.create(
                        solicitante = persona,
                        monto = form.cleaned_data['monto'],
                        )
            conexion = aprobar_solicitud(persona.numero_documento)
            print(conexion)
            if conexion['estado']:
                prestamo.aprobado = True
                prestamo.save()
                messages.add_message(request, messages.SUCCESS, 'Aprobado')
            else:
                if conexion['errores']:
                    prestamo.errores = True
                    messages.add_message(request, messages.ERROR, 'NO Aprobado')
                elif conexion['error_conexion']:
                    #si no podemos conectarnos al API externa
                    messages.add_message(request, messages.ERROR,
                        'Error al procesar la solicitud. Vuelva a intentarlo')
                    prestamo.delete()
                    persona.delete()
                else:
                    messages.add_message(request, messages.ERROR, 'NO Aprobado')

            return redirect('solicitud')
        else:
            messages.add_message(request, messages.WARNING,
                'Error en los datos, corrija el formulario')
    else:
        form = SolicitudForm()

    return render(request, 'registros/solicitud.html', {'form': form})


class SolicitudListView(PermissionRequiredMixin, ListView):
    """
    Lista de solicitudes de prestamos
    """
    model = Prestamo
    permission_required = 'is_superuser'
    paginate_by = 10  # pagination


class SolicitudDetailView(PermissionRequiredMixin, DetailView):
    """
    Detalle de una solicitud
    """
    model = Prestamo
    permission_required = 'is_superuser'


class SolicitudUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Prestamo
    permission_required = 'is_superuser'
    fields =  '__all__'
    success_message = "Actualizado con éxito"

    def get_success_url(self):
        return reverse(
            "lista_solicitudes"
        )


class SolicitudDeleteView(PermissionRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Prestamo
    success_url = reverse_lazy('lista_solicitudes')
    success_message = "Solicitud eliminada"
    permission_required = 'is_superuser'

    def delete(self, request, *args, **kwargs):
        # Fix para mostrar success_message
        messages.success(self.request, self.success_message)
        return super(SolicitudDeleteView, self).delete(request, *args, **kwargs)
