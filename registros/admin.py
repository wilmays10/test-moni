from django.contrib import admin
from registros.models import Prestamo, Persona


admin.site.register(Persona)
admin.site.register(Prestamo)