from django import forms
from model_utils import Choices


class SolicitudForm (forms.Form):
    nombre = forms.CharField( 
        min_length=4, 
        max_length=30,
        widget= forms.TextInput(
                attrs= {
                    'placeholder':"Nombre"
                }
            )
        )
    apellido = forms.CharField (
        min_length =2, 
        max_length = 50,
        widget= forms.TextInput(
                attrs= {
                    'placeholder':'Apellido'
                }
            )
        )
    genero = forms.ChoiceField(
        choices=Choices("masculino", "femenino")
        )
    dni = forms.IntegerField(
        widget= forms.TextInput(
                attrs= {
                    'placeholder':'Número de documento'
                }
            )
        )
    email = forms.EmailField(
        min_length=6, 
        max_length= 70, 
        widget=forms.EmailInput(
            attrs= {
                'placeholder':'Email'
                }
            )
        )
    monto = forms.IntegerField(
        widget= forms.TextInput(
                attrs= {
                    'placeholder':'Monto'
                }
            )
        )
